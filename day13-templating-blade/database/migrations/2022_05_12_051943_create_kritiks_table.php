<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKritiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritiks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content');
            $table->integer('point');

            //Create foreign key user_id
            $table->unsignedBigInteger('user_id');
 
            $table->foreign('user_id')->references('id')->on('users');

            //Create foreign key film_id
            $table->unsignedBigInteger('film_id');
        
            $table->foreign('film_id')->references('id')->on('films');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kritiks');
    }
}

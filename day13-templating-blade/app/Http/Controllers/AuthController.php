<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register(){
        return view('register');
    }

    function welcome(Request $request){
        $firstname = $request['fname'];
        $lastname = $request['lname'];
        return view('welcome', [
            "firstname" => $firstname,
            "lastname" => $lastname
        ]);
    }
}

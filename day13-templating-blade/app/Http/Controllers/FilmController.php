<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use DB;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::all();
        return view('film.index', compact('films'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genres')->get();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|min:5',
            'ringkasan' => 'required',
            'year' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ],
        [
            'judul.required' => 'judul harus diisi',
            'judul.min' => 'Panjang judul minimal 5 karakter',
            'ringkasan.required' => 'ringkasan harus diisi',
            'year.required' => 'year harus diisi',
            'poster.required' => 'poster harus diisi',
            'genre_id.required' => 'genre_id harus diisi'
        ]
        );

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->year = $request->year;
        $film->poster = $request->poster;

        $film->genre_id = $request->genre_id;

        $film->save();

        return redirect('film') -> with('success', 'Film berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = DB::table('genres')->get();
        return view('film.edit', compact('film', 'genre')) -> with('success', 'Film berhasil disimpan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|min:5',
            'ringkasan' => 'required',
            'year' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ],
        [
            'judul.required' => 'judul harus diisi',
            'judul.min' => 'Panjang judul minimal 5 karakter',
            'ringkasan.required' => 'ringkasan harus diisi',
            'year.required' => 'year harus diisi',
            'poster.required' => 'poster harus diisi',
            'genre_id.required' => 'genre_id harus diisi'
        ]
        );

        $film = Film::find($id);
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->year = $request->year;
        $film->poster = $request->poster;

        $film->genre_id = $request->genre_id;

        $film->save();

        return redirect('film') -> with('success', 'Film berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $film->delete();

        return redirect('film') -> with('success', 'Film berhasil dihapus');
    }
}

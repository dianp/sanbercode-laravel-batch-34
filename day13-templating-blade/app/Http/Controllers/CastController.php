<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('cast.create');
    }
    
    public function store(Request $request){
        
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'nama.min' => 'Panjang nama minimal 5 karakter',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Bio harus diisi'
        ]
        );
        
        $query = DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        
        return redirect('cast') -> with('success', 'Cast berhasil disimpan');
        
    }
    
    public function index(){
        $casts = DB::table('casts')-> get();
        return view('cast.index', compact('casts', $casts));
    }

    public function show($cast_id){
        $cast = DB::table('casts')-> where('id', $cast_id)->first();
        return view('cast.show', compact('cast', $cast));
    }

    public function edit($cast_id){
        $cast = DB::table('casts')-> where('id', $cast_id)->first();
        return view('cast.edit', compact('cast', $cast));
    }

    public function update($cast_id, Request $request){
        
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'nama.min' => 'Panjang nama minimal 5 karakter',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Bio harus diisi'
        ]
        );
        
        $query = DB::table('casts')
                ->where('id', $cast_id)
                ->update([
                    'nama' => $request["nama"],
                    'umur' => $request["umur"],
                    'bio' => $request["bio"]
                ]);
        
        return redirect('cast') -> with('success', 'Cast berhasil diupdate');
        
    }

    public function destroy($cast_id){
        $query = DB::table('casts')->where('id', $cast_id)->delete();
        return redirect('cast') -> with('success', 'Cast berhasil dihapus');
    }
}

@extends('adminlte.master')

@section('title')
Register
@endsection


@section('content')
    <h2>Buat Account Baru</h2>
    <h5>Sign Up Form</h5>

    <form action="/welcome" method="post">
        @csrf

        <label>First name:</label><br><br>
        <input type="text" name="fname">
        <br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lname">
        <br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="male" id="male">
        <label for="male">Male</label>
        <br>
        <input type="radio" name="gender" value="female" id="female">
        <label for="female">Female</label>
        <br>
        <input type="radio" name="gender" value="other" id="other-gender">
        <label for="other-gender">Other</label>
        <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="non-indonesian"> Non Indonesian</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa[]" value="bahasa" id="bahasa">
        <label for="bahasa">Bahasa Indonesia</label>
        <br>
        <input type="checkbox" name="bahasa[]" value="english" id="english">
        <label for="english">English</label>
        <br>
        <input type="checkbox" name="bahasa[]" value="other-language" id="other-language">
        <label for="other-language">Other</label>
        <br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
        
    </form>

@endsection

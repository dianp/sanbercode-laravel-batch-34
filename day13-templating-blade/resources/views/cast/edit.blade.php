@extends('adminlte.master')


@section('title')
Edit
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Edit Cast Data</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{old('nama', $cast->nama)}}" id="exampleInputEmail1" placeholder="Nama" >
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="exampleInputPassword1">Umur</label>
            <input type="number" class="form-control" name="umur" value="{{old('umur', $cast->umur)}}" id="exampleInputPassword1" placeholder="Umur" >
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea type="text" class="form-control" name="bio" rows="3" placeholder="Enter Bio...." >{{old('bio', $cast->bio)}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>

@endsection
@extends('adminlte.master')


@section('title')
Index
@endsection

@section('content')

<div class="card">
    <div class="card-header">
    <h3 class="card-title">Expandable Table</h3>
    </div>
    <!-- ./card-header -->
    <div class="card-body">
    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif
    <a href="/cast/create" class="btn btn-info mb-3">Add Cast</a>

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse($casts as $key => $cast)
            <tr>
                <td class="">{{$key + 1}}</td>
                <td class="">{{$cast->nama}}</td>
                <td class="">{{$cast->umur}}</td>
                <td class=" overflow-scroll h-25">{{$cast->bio}}</td>
                <td class="d-flex">
                    <a href='/cast/{{$cast->id}}' class='btn btn-info btn-sm'>
                        <i class="fas fa-user"></i>  Detail
                    </a>
                    <a href='/cast/{{$cast->id}}/edit' class='btn btn-warning btn-sm mx-2'>
                        <i class="far fa-edit"></i>  Edit
                    </a>
                    <form action="/cast/{{$cast->id}}" method="post">
                        @csrf 
                        @method('delete')
                        <button type="submit" class='btn btn-danger btn-sm'>
                            <i class="far fa-trash-alt"></i> Delete
                        </button>
                    </form>
                </td>
            <tr>
        @empty
            <tr>
                <td colspan="5" class="text-center">
                    No Cast Data
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection
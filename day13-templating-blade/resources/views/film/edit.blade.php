@extends('adminlte.master')


@section('title')
Create
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create Film Data</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/film/{{$film->id}}" method="post">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
            <label for="exampleInputEmail1">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{old('judul', $film->judul)}}" id="exampleInputEmail1" placeholder="judul" >
            @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label>Ringkasan</label>
                <textarea type="text" class="form-control" name="ringkasan" rows="3" placeholder="Enter ringkasan...." >{{old('ringkasan', $film->ringkasan)}}</textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
            <label for="exampleInputPassword1">Year</label>
            <input type="number" class="form-control" name="year" value="{{old('year', $film->year)}}" id="exampleInputPassword1" placeholder="year" >
            @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Poster</label>
                <input type="text" class="form-control" name="poster" value="{{old('poster', $film->poster )}}" id="exampleInputEmail1" placeholder="poster" >
                @error('poster')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Genre</label>
                <select name="genre_id" id="exampleInputEmail1">
                    <option value=null>Null</option>
                    @foreach($genre as $gen)
                        <option value="{{$gen->id}}">{{$gen->nama}}</option>
                    @endforeach
                  </select>
                @error('genre')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>

@endsection
@extends('adminlte.master')


@section('title')
Show
@endsection

@section('content')

<a href='/film' class='btn btn-info btn-sm mb-3'>
    <i class="fas fa-long-arrow-alt-left"></i> Show All film
</a>
<div class="card border-dark p-3 col-4" >
    <div class="card-header text-muted text-center ">
        <h3>film Info</h3>
    </div>
    <div class="card-body">
        <div class="row">
        <div class="col-6">
            <h2 class="lead"><b>{{$film->judul}} {{$film->year}}</b></h2>
            <p class="text-muted text"><b>{{$film->poster}}</b></p>
            
            <p class="text-muted text-sm">Ringkasan :<br>{{$film->ringkasan}}</p>
        </div>
        <div class="col-6 text-center">
            <img src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="user-avatar" class="img-circle img-fluid">
        </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="text-left d-flex">
            <a href="{{$film->id}}/edit" class="btn btn-sm btn-warning mx-2">
                <i class="fas fa-edit"></i> Edit
            </a>
            <form action="/film/{{$film->id}}" method="post">
                @csrf 
                @method('delete')
                <button type="submit" class='btn btn-danger btn-sm'>
                    <i class="far fa-trash-alt"></i> Delete
                </button>
            </form>
        </div>
    </div>
</div>
              
@endsection
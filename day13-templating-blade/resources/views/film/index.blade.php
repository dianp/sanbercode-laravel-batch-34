@extends('adminlte.master')


@section('title')
Index
@endsection

@section('content')

<div class="card">
    <div class="card-header">
    <h3 class="card-title">Table</h3>
    </div>
    <!-- ./card-header -->
    <div class="card-body">
    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif
    <a href="/film/create" class="btn btn-info mb-3">Add film</a>

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Judul</th>
            <th>Ringkasan</th>
            <th>Year</th>
            <th>Poster</th>
            <th>Genre</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse($films as $key => $film)
            <tr>
                <td class="">{{$key + 1}}</td>
                <td class="">{{$film->judul}}</td>
                <td class="">{{$film->ringkasan}}</td>
                <td class="">{{$film->year}}</td>
                <td class="">{{$film->poster}}</td>
                <td class="">{{$film->genre_id}}</td>
                <td class="d-flex">
                    <a href='/film/{{$film->id}}' class='btn btn-info btn-sm'>
                        <i class="fas fa-user"></i>  Detail
                    </a>
                    <a href='/film/{{$film->id}}/edit' class='btn btn-warning btn-sm mx-2'>
                        <i class="far fa-edit"></i>  Edit
                    </a>
                    <form action="/film/{{$film->id}}" method="post">
                        @csrf 
                        @method('delete')
                        <button type="submit" class='btn btn-danger btn-sm'>
                            <i class="far fa-trash-alt"></i> Delete
                        </button>
                    </form>
                </td>
            <tr>
        @empty
            <tr>
                <td colspan="6 " class="text-center">
                    No film Data
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection